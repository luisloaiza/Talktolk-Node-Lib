const Socket = require('net');
const events = require('events');

const SocketTCP = function(host,port){

     this.debug = false;
     this.HOST  = host;
     this.PORT  = port;
     this.socket;

     this.loggeIn;
     this.monkey_id=null;

     this.connect = function(onSuccess){

          var self=this;

          this.socket = new Socket.Socket();
          this.socket.connect(this.PORT,this.HOST,function(error){
                onSuccess(error);
          });

          this.socket.on('error',function(data){
              console.log(" ERROR socket connection "+data);
              self.emit('onError', data);
          });
          this.socket.on('data',function(data){

            var message = new MessageBuffer(data.length);
            message.setBuffer(data);
            var code = message.getByte();

            var command = data[2];

            switch(command){
                case SimpleSgsProtocol.LOGIN_REQUEST :
                    var reason = message.getString();

                    break;

                case SimpleSgsProtocol.LOGIN_SUCCESS :
                    self.emit('onLogin', message);
                    break;

                case SimpleSgsProtocol.LOGIN_FAILURE :
                    var reason = message.getString();
                    self.emit('onError', reason);
                    break;

                case SimpleSgsProtocol.PONG_CONNECTION :
                    self.emit('onPong', message);
                break;

                case SimpleSgsProtocol.SESSION_MESSAGE :
                    var stringMessage=message.getStringBody().replace(/\0/g, '');

                    var messageObj=self.parseMessage(stringMessage);

                    if(messageObj!=null)
                      self.emit('onMessage', messageObj);
                    break;

                case SimpleSgsProtocol.LOGOUT_SUCCESS :{

                    self.emit('onLogout', message);
                }
                break;
            }

        });
     }
     this.parseMessage =  function(stringMessage){
        if(stringMessage.length==0)
          return null;

        var messageObj=null;
          try{
              messageObj=JSON.parse(stringMessage.trim());
          }
          catch(e){

//              stringMessage=stringMessage.replace(/\0/g, '');

               stringMessage=stringMessage.substr(1);
               console.log("ERROR :"+e.toString()+" end "+stringMessage);
               return this.parseMessage(stringMessage);
          }

        return messageObj;
     }

     this.close = function(){
          console.log("Desconectando Socket..!!!");
          if(this.socket){
              this.socket.destroy();
          }
        this.emit('onLogout');
     }

     this.login = function(username,password){
        this.monkey_id=username;
         var len = 2 + MessageBuffer.getSize(username) + MessageBuffer.getSize(password);
         var message = new MessageBuffer(len+2);
         message.putShort(len);
         message.putByte(SimpleSgsProtocol.LOGIN_REQUEST);
         message.putByte(SimpleSgsProtocol.VERSION);
         message.putString(username);
         message.putString(password);

         this.postRequest(message.buffer);

     }

     this.postRequest = function(buffer){
          this.socket.write(buffer);
     }

     this.logout = function(){
        var len = 1;
        var message = new MessageBuffer(2 + len);
        message.putShort(len);
        message.putByte(SimpleSgsProtocol.LOGOUT_REQUEST);
        this.postRequest(message);
     }

     this.ping = function(){
        var len = 1;
        var messageBuffer = new MessageBuffer(2 + len);
        messageBuffer.putShort(len);
        messageBuffer.putByte(SimpleSgsProtocol.PING_CONNECTION);
        this.postRequest(messageBuffer.buffer);
     }

     this.sendMessage = function(message){
        //var mssgString = JSON.stringify(message);
        var messagesBytes = getBytes(message);
        var len = 1 + messagesBytes.length;

        var messageBuffer = new MessageBuffer(2 + len);
        messageBuffer.putShort(len);
        messageBuffer.putByte(SimpleSgsProtocol.SESSION_MESSAGE);
        messageBuffer.putBytes(messagesBytes);
        this.postRequest(messageBuffer.buffer);
     }


     events.EventEmitter.call(this);
     return this;
}



var SimpleSgsProtocol = {

     MAX_MESSAGE_LENGTH : 65535,

     MAX_PAYLOAD_LENGTH : 65532,

     VERSION : 0x05,

     LOGIN_REQUEST : 0x10,

     LOGIN_SUCCESS : 0x11,

     LOGIN_FAILURE : 0x12,

     LOGIN_REDIRECT : 0x13,

     RECONNECT_REQUEST : 0x20,

     RECONNECT_SUCCESS : 0x21,

     RECONNECT_FAILURE : 0x22,

     SESSION_MESSAGE : 0x30,

     PING_CONNECTION : 0x31,

     PONG_CONNECTION : 0x32,

     LOGOUT_REQUEST : 0x40,

     LOGOUT_SUCCESS : 0x41,

     CHANNEL_JOIN : 0x50,

     CHANNEL_LEAVE : 0x51,

     CHANNEL_MESSAGE : 0x52
}

var MessageBuffer = function(length){

    this.buffer = new Buffer(length);
    this.capacity;
    this.pos=0;
    this.limit;

     this.getCapacity = function(){
          return this.capacity;
     }

     this.setCapacity = function(pcapacity){
          this.capacity = pcapacity;
     }

     this.getPosition = function(){
          return this.pos;
     }

     this.setPosition= function(ppos){
          this.pos = ppos;
     }

     this.rewind = function(){
        this.pos = 0;
    }

    this.putByte = function(int_value){
     //console.log("putByte..!!!");
        //console.log("putByte pos:"+this.pos);
     this.buffer[this.pos++] = int_value;
     this.limit = (this.pos == this.capacity ? this.pos : this.pos + 1);
    }

    this.putByteArray=function(bytesArray){
        if (this.pos + 2 + bytesArray.length > this.capacity) {
            throw new IndexOutOfBoundsException();
        }
        this.putShort(bytesArray.length);
        this.putBytes(bytesArray);
    }

    this.putBytes = function(bytesArray){
        if (this.pos + bytesArray.length > this.capacity) {
            throw new IndexOutOfBoundsException();
        }
        for (var i = 0; i < bytesArray.length; i++) {
            this.putByte(bytesArray[i]);
        }
    }

    this.putChar = function(int_value){
        if (this.pos + 2 > this.capacity) {
            throw new IndexOutOfBoundsException();
        }
        putByte((int_value >>> 8) & 0xFF);
        putByte((int_value >>> 0) & 0xFF);
    }

    this.putShort= function(int_value) {

        if (this.pos + 2 > this.capacity){
            //throw new IndexOutOfBoundsException();
            console.log(" throw new IndexOutOfBoundsException() ");
        }
        this.putByte((int_value >>> 8) & 0xFF);
        this.putByte((int_value >>> 0) & 0xFF);
    }

    this.putInt = function(int_value){
         if (this.pos + 4 > this.capacity) {
            throw new IndexOutOfBoundsException();
        }
        putByte((int_value >>> 24) & 0xff);
        putByte((int_value >>> 16) & 0xff);
        putByte((int_value >>> 8) & 0xff);
        putByte((int_value >>> 0) & 0xff);
    }

    /*falta putLong function */

    this.putString = function(string){

          var size = this.getSize(string);



          if (this.pos + size > this.capacity) {
            throw new IndexOutOfBoundsException();
          }

          this.putShort(size - 2);

          var strlen = string.length;

          for (var i = 0; i < strlen; i++) {
            var c = string.charCodeAt(i);
            if (!((c >= 0x0001) && (c <= 0x007F))) {
                break;
            }
            this.buffer[this.pos++] = c;
          }

        for (; i < strlen; i++) {
            var c = string.charCodeAt(i);
            if ((c >= 0x0001) && (c <= 0x007F)){
                this.buffer[this.pos++] = c;
            } else if (c > 0x07FF) {
                this.buffer[this.pos++] = (0xE0 | ((c >> 12) & 0x0F));
                this.buffer[this.pos++] = (0x80 | ((c >> 6) & 0x3F));
                this.buffer[this.pos++] = (0x80 | ((c >> 0) & 0x3F));
            } else {
                this.buffer[this.pos++] = (0xC0 | ((c >> 6) & 0x1F));
                this.buffer[this.pos++] = (0x80 | ((c >> 0) & 0x3F));
            }
        }

       this.limit = (this.pos == this.capacity ? this.pos : this.pos + 1);
    }

    this.getByte=function(){
        //console.log("Pos :"+this.pos+" Limit :"+this.limit);
        if (this.pos == this.limit) {
          //  console.log("pos == limit");
            throw new IndexOutOfBoundsException();
        }
        var b = this.buffer[this.pos++];

        return b;
    }

    this.getSize = function(string){

        var utfLen = 0;

        for (var i = 0; i < string.length; i++) {
            //var c = string.charAt(i);
            var c = string.charCodeAt(i);
            if ((c >= 0x0001) && (c <= 0x007F)) {
                utfLen++;
                //console.log("+1");
            } else if (c > 0x07FF) {
                utfLen += 3;
                //console.log("+3");
            } else {
                utfLen += 2;
                //console.log("+2");
            }
        }
        return utfLen + 2;
    }

    this.setBuffer = function(buffer){
        if(buffer.length < this.buffer.length){
            //console.log("Exception length...!!!");
        }

        this.limit = buffer.length;
        this.capacity = buffer.length;

        for (var i = 0; i < buffer.length; i++) {
            this.buffer[i] = buffer[i];
        };
    }

    this.getUnsignedShort = function(){
        if (this.pos + 2 > this.limit) {
            throw new IndexOutOfBoundsException();
        }

        return ((this.getByte() & 255) << 8) + ((this.getByte() & 255) << 0);
    }

     this.getStringBody = function() {
        //should start from the index 3
        // Note: code adapted from java.io.DataInputStream.readUTF

        if (this.pos + 2 > this.limit) {
            throw ("Error Limit getString...!!!");
        }

        var savePos = this.pos;

        /*
         * Get length of UTF encoded string.
         */
        var utfLen = this.getUnsignedShort();
        var utfEnd = utfLen + this.pos;
        if (utfEnd > this.limit) {
            this.pos = savePos;
            //console.log("Get length of UTF encoded string: Error Limit getString...!!! "+utfEnd +" - "+ this.limit);
        }

        /*
         * Decode string.
         */
        var chars = [];
        var c, char2, char3;
        var index = 0;

        var myPos=3;
        while (myPos < this.limit) {
            c = this.buffer[myPos];// & 0xff;
            /*if (c > 160) {// changed from 127 to 160 -  need to figure out better solutions
                break;
            }*/
            myPos++;
            chars[index++] = String.fromCharCode(c);
        }
        this.pos = myPos;
        // The number of chars produced may be less than utfLen
        var string="";
        for (var i = 0; i < chars.length; i++) {
            string+=chars[i];
        };

        //return chars;
        return string;
    }


    this.getString = function() {

        // Note: code adapted from java.io.DataInputStream.readUTF

        if (this.pos + 2 > this.limit) {
            throw ("Error Limit getString...!!!");
        }

        var savePos = this.pos;

        /*
         * Get length of UTF encoded string.
         */
        var utfLen = this.getUnsignedShort();
        var utfEnd = utfLen + this.pos;
        if (utfEnd > this.limit) {
            this.pos = savePos;
            //console.log("Get length of UTF encoded string: Error Limit getString...!!! "+utfEnd +" - "+ this.limit);
        }

        /*
         * Decode string.
         */
        var chars = [];
        var c, char2, char3;
        var index = 0;

        while (this.pos < utfEnd) {
            c = this.buffer[this.pos] & 0xff;
            if (c > 160) {// changed from 127 to 160 -  need to figure out better solutions
                break;
            }
            this.pos++;
            chars[index++] = String.fromCharCode(c);
        }


        try {
            while (this.pos < utfEnd) {
                c = this.buffer[this.pos] & 0xff;

                switch (c >> 4) {

                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        /* 0xxxxxxx*/
                        this.pos++;
                        chars[index++] = String.fromCharCode(c);
                        break;

                    case 12:
                    case 13:
                        /* 110x xxxx   10xx xxxx*/
                        this.pos += 2;
                        if (this.pos > utfEnd) {
                            throw ("malformed input: partial character at end");
                        }
                        char2 = this.buffer[this.pos - 1];
                        if ((char2 & 0xC0) != 0x80) {
                            throw (" char2 - "+char2+" malformed input around byte " + (this.pos-1));
                        }
                         var string = (((c & 0x1F) << 6) | (char2 & 0x3F));
                         chars[index++] = String.fromCharCode(string);
                        break;

                    case 14:
                        /* 1110 xxxx  10xx xxxx  10xx xxxx */
                        this.pos += 3;
                        if (this.pos > utfEnd) {
                            throw ("malformed input: partial character at end");
                        }
                        char2 = this.buffer[this.pos - 2];
                        char3 = this.buffer[this.pos - 1];
                        if (((char2 & 0xC0) != 0x80) || ((char3 & 0xC0) != 0x80)) {
                            throw ("malformed input around byte " + (this.pos - 1));
                        }

                        var string = (((c & 0x0F) << 12)|((char2 & 0x3F) << 6)|((char3 & 0x3F) << 0));
                        chars[index++] = String.fromCharCode(string);
                        break;

                    default:
                        /* 10xx xxxx,  1111 xxxx */
                        throw new Error("malformed input around byte " + this.pos);
                }
            }

        }catch(e) {
            // restore position
            this.pos = savePos;
            console.log("Error cocurrido :"+e);
        }
        // The number of chars produced may be less than utfLen
        var string="";
        for (var i = 0; i < chars.length; i++) {
            string+=chars[i];
        };

        //return chars;
        return string;
    }

    return this;

}

MessageBuffer.getSize = function(string){

     var utfLen = 0;

    for (var i = 0; i < string.length; i++) {
        //var c = string.charAt(i);
        var c = string.charCodeAt(i);
        if ((c >= 0x0001) && (c <= 0x007F)) {
            utfLen++;

        } else if (c > 0x07FF) {
            utfLen += 3;

        } else {
            utfLen += 2;

        }
    }
    return utfLen + 2;
}

function getBytes(str){
  var ch, st, re = [];

  for (var i = 0; i < str.length; i++ ) {
    ch = str.charCodeAt(i);  // get char
    st = [];                 // set up "stack"
    do {
      st.push(ch & 0xFF);
      //console.log("character :"+str.charAt(i)+" byte:"+(ch & 0xFF));  // push byte to stack
      ch = ch >> 8;          // shift value down by 1 byte
    }
    while ( ch );
    // add stack contents to result
    // done because chars have "wrong" endianness

    re = re.concat( st.reverse());
  }
  // return an array of bytes
  return re;
}

SocketTCP.prototype.__proto__ = events.EventEmitter.prototype;

String.prototype.replaceAll = function(str1, str2, ignore) {
  return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof(str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
}

module.exports = SocketTCP;









/*END OF FILE*/
