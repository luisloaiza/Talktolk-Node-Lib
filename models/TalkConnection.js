
//const async = require("async")
const SocketTCP = require("../lib/SocketTalk.js");
const TalkEnums = require("../lib/TalkEnum.js");
const EventEmitter = require('events');

// check to get from env vars
const HOST_MONKEY_SOCKET_SERVER = "channel.talktolk.com"
const PORT_MONKEY_SOCKET_SERVER= 80


const EVENTS={
  MESSAGE_EVENT : 'Message',
  MESSAGE_SYNC_EVENT : 'MessageSync',
  STATUS_CHANGE_EVENT : 'StatusChange',
  PONG_EVENT : 'PongEvent'

  // const MESSAGE_FAIL_EVENT = 'MessageFail';
  // const ACKNOWLEDGE_EVENT = 'Acknowledge';
  // const NOTIFICATION_EVENT = 'Notification';
  //
  // const GROUP_CREATE_EVENT = 'GroupCreate';
  // const GROUP_ADD_EVENT = 'GroupAdd';
  // const GROUP_REMOVE_EVENT = 'GroupRemove';
  // const GROUP_LIST_EVENT = 'GroupList';
  // const GROUP_INFO_UPDATE_EVENT = 'GroupInfoUpdate';
  //

  //
  // const CONNECT_EVENT = 'Connect';
  // const DISCONNECT_EVENT = 'Disconnect';
  //
  // const CONVERSATION_OPEN_EVENT = 'ConversationOpen';
  // const CONVERSATION_STATUS_CHANGE_EVENT = 'ConversationStatusChange'; //status referes to online / last seen
  // const CONVERSATION_CLOSE_EVENT = 'ConversationClose';


}

class TalkConnection {

      constructor(appId, appSecret,uid, apiService, lastTimeSynced = 0,  isLogicPing = true, autoSync=false){

        this.on = this.alias('addListener');
        this.enums= new TalkEnums();
        this.uid=uid
        this.apiService = apiService
        this.appId = appId
        this.appSecret = appSecret
        this.lastTimeSynced = lastTimeSynced // get from database

        this.isLogicPing = isLogicPing
        const self = this

        console.log(" Connecting user with id "+uid);

        this.socketInstance = new SocketTCP(HOST_MONKEY_SOCKET_SERVER, PORT_MONKEY_SOCKET_SERVER);
        this.socketInstance.on("onLogin",function(data){
          console.log(uid," onLogin ");
          self.changeStatus(self.enums.Status.ONLINE)
          self._getEmitter().emit("onLogin", this.status);
          self.checkAvailability()
        });
        this.socketInstance.on("onLogout",function(data){
          self.changeStatus(self.enums.Status.LOGOUT)
          self._getEmitter().emit("onLogout", this.status);
        });
        this.socketInstance.on("onError",function(data){
          console.log(uid," onError ", data);
          self.changeStatus(self.enums.Status.LOGOUT, data)
        });

        this.socketInstance.on("onMessage",function(data){

          if(data.args.type ==3 ){ // logic pong
            self.lastPongTime = new Date().getTime()
            //console.log(" MirrorPong ",self.lastPongTime);
            return
          }

          self.processMessage(data.args)
        });// end of onMEssage

        this.socketInstance.on("onPong",function(data){

          self.lastPongTime = new Date().getTime()
          console.log(uid," onPong ",self.lastPongTime);
          const messagePong={cmd:100, args:{timestamp:self.lastPongTime}};
          self._getEmitter().emit(EVENTS.PONG_EVENT, messagePong);
        });

        this.connect(autoSync)

      return this
    }

    connect(autoSync=false){

      //disconnect socket if it's already connected
        if (this.socketInstance != null) {

          this.socketInstance.onclose = function(){};
          this.socketInstance.close();
          //this.socketInstance = null;
        }
        if(this.availabilityWatchdog){
          clearInterval(this.availabilityWatchdog)
          this.availabilityWatchdog =  null
        }
        this.lastPongTime = 0


        const self =  this
        this.changeStatus(this.enums.Status.CONNECTING)
        this.socketInstance.connect(function(error){

          if(error==null){
            console.log("SocketTCP connected...!!!");
          }
          else{
            console.log("SocketTCP Error Connected...!!!"+error);
            self.changeStatus(self.enums.Status.LOGOUT)
            return
          }

          const password=`${self.appId}:${self.appSecret}`
          // Addint in password string the not zombie mode=0 and the websocketSessionId that is used for the socket server
          self.socketInstance.login(self.uid,password); //.concat(":").concat("0").concat(":").concat(websocketSessionId)
        }) // end connect funcion in socket


        if(autoSync){
          this.syncMessages(50);
        }


    }


     _processSyncMessages(messages, remaining){

      this._processMultipleMessages(messages);

      if (remaining > 0) {
        this.syncMessages(50); // keep bringin more messages
      }else if(this.status!==this.enums.Status.ONLINE){
        this.connect();
      }else{
        //this._getEmitter().emit(STATUS_CHANGE_EVENT, this.status);
      }
    }

    _processMultipleMessages(messages){
      const self =  this
      messages.map(function(message){
        //let msg = new MOKMessage(this.enums.ProtocolCommand.MESSAGE, message);
        self.processMessage(message);
      });
    }

    processMessage(message){

      if (message.id > 0 && message.datetime > this.lastTimeSynced ) {
        this.lastTimeSynced = Math.trunc(message.datetime);
      }

      if( (typeof message.params) == "string"){
        try{
            message.params = JSON.parse(message.params)
        }
        catch(e){
          message.params = {}
        }

      }


      console.log(this.lastTimeSynced,"last time : receiving messages ",message)
      this._getEmitter().emit(EVENTS.MESSAGE_EVENT, message);
    }

    syncMessages(page){

      this.changeStatus(this.enums.Status.SYNCING)

      const timeSynced = Math.trunc(this.lastTimeSynced)
      const url =`/user/messages/${this.uid}/${timeSynced}/${page}`
      const self = this
      console.log("syncing ",url)

      this.apiService.basicRequest("GET", url,
       null, (err, response)=>{

         if(err){
           console.log('Monkey - Sync - Error... '+err);
           setTimeout(function(){
             self.syncMessages(page);
           }, 2000 );
           return;
         }
         console.log("SYNC Response ",response)
         if(!response || !response.data){
           console.log( ' Sync - Empty Response');
           setTimeout(function(){
             self.syncMessages(page);
           }, 2000 );
           return ;
         }

         let data = response.data;

         if(data.messages.length > 0){
           this._processSyncMessages(data.messages, data.remaining)
         }else if(!this.isOnline()){
           this.connect();
         }

      });
    }

    isOnline(){
      return this.status==this.enums.Status.ONLINE
    }



  checkAvailability(){
    // chekc with PONG command the current connection
    const self = this
    if(this.availabilityWatchdog){
      console.log("Already running watchdog ... ")
      return
    }

    this.availabilityWatchdog = setInterval(function(){

      if(self.lastPongTime>0){

        const currentTime =  new Date().getTime()
        if(TalkConnection.isTimedOut(currentTime,self.lastPongTime,0.16)) // 10 seconds
        {
          console.log(currentTime," Connection timed out with last pong ... ",new Date(self.lastPongTime)
          ," ============= Reconnecting =============")
          return self.connect()
        }

      }

      self.sendPing()
    }, 5000 );
  }

  static isTimedOut(timestamp1, timestamp2, minutes) {

    const difference = timestamp1 - timestamp2;
    let minDifference = (difference/1000/60).toFixed(2);
    return minDifference>minutes;
  }

  stopListening(){
      this.socketInstance.close();
  }
  sendPing(){
    if(this.isLogicPing){
      this.sendMirrorPong()
    }
    else{
      self.socketInstance.ping()
    }
  }
  sendMirrorPong(){

    let args = {
      app_id: this.appId,
      id: this.generateRandomMessageId(),
      sid: this.uid,
      rid: this.uid,
      type: 3,
      msg: "ping",
      //params:{}
      datetime: Math.round(new Date().getTime() / 1000)
    };

    const msg = {
      cmd:200,
      args:args
    }

    this.sendMessage(msg)
  }

  generateRandomMessageId(){
    return Math.round(new Date().getTime() / 1000) * -1;
  }

  sendMessage(message){
    this.socketInstance.sendMessage(JSON.stringify(message));
  }
  changeStatus(newStatus){
    this.status = newStatus;
    this._getEmitter().emit(EVENTS.STATUS_CHANGE_EVENT, this.status);
  }

  _getEmitter() {
    if (this.emitter == null) {
      this.emitter = new EventEmitter();
    }
    return this.emitter;
  }

  addListener(evt, callback){
    let emitter = this._getEmitter();
    emitter.addListener(evt, callback);

    return this;
  }

  removeEvent(evt){
    let emitter = this._getEmitter();
    emitter.removeEvent(evt);
    return this;
  }

  alias(name) {
    return function aliasClosure() {
      return this[name].apply(this, arguments);
    };
  }

}

module.exports = TalkConnection;
